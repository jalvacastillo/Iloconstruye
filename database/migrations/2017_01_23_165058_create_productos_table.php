<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->increments('id');

            $table->string('nombre')->nullable();
            $table->string('img');
            $table->text('descripcion')->nullable();
            $table->mediumText('detalle')->nullable();

            $table->decimal('precio', 6,2)->default(0);
            $table->integer('categoria_id')->unsigned()->index();
            $table->string('etiquetas')->nullable();
            
            $table->boolean('activo')->default(1);
            $table->integer('orden')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
