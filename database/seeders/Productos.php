<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Productos\Producto;
use App\Models\Servicios\Servicio;


class Productos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();


        for($i = 1; $i <= 10 ; $i++)
        {
            $table = new Producto;

            $table->nombre          = $faker->name;
            // $table->img             = 'productos/default.jpg';
            $table->img             = 'productos/' . $i . '.png';
            $table->descripcion     = $faker->text;
            $table->detalle         = $faker->text;
            $table->precio          = $faker->numberBetween(10,200);
            $table->categoria_id    = $faker->numberBetween(1,7);
            $table->activo          = 1;
            $table->orden           = $i;
            $table->save();
            
        }

        for($i = 1; $i <= 4 ; $i++)
        {
            $table = new Servicio;

            $table->nombre          = $faker->name;
            $table->img             = 'servicios/default.jpg';
            $table->descripcion     = $faker->text;
            $table->detalle         = $faker->text;
            $table->precio          = $faker->numberBetween(10,200);
            $table->activo          = 1;
            $table->orden           = $i;
            $table->save();
            
        }
    
    }

}
