<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Admin\Categoria;

class Categorias extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        Categoria::create(['nombre' => 'Arreglos', 'img' => 'categorias/default.jpg', 'orden' => 1]);
        Categoria::create(['nombre' => 'Decoraciones', 'img' => 'categorias/default.jpg', 'orden' => 2]);
        Categoria::create(['nombre' => 'Detalles', 'img' => 'categorias/default.jpg', 'orden' => 3]);
        Categoria::create(['nombre' => 'Chocolate', 'img' => 'categorias/default.jpg', 'orden' => 4]);
        Categoria::create(['nombre' => 'Tarjetas', 'img' => 'categorias/default.jpg', 'orden' => 5]);
        Categoria::create(['nombre' => 'Box', 'img' => 'categorias/default.jpg', 'orden' => 6]);
        Categoria::create(['nombre' => 'Globos', 'img' => 'categorias/default.jpg', 'orden' => 7]);
        
    }
}
