<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Registros\Direccion;

class UserSeeder extends Seeder
{

    public function run()
    {

        $faker = \Faker\Factory::create();

            $user = new User;
            $user->name = 'Usuario Admin';
            $user->email = 'admin@admin.com';
            $user->password = Hash::make('admin');
            $user->save();


    }
}
