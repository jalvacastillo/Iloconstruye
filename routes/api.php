<?php

// use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


require base_path('routes/modulos/auth.php');
        
// Route::group(['middleware' => ['jwt.auth']], function () {


        require base_path('routes/modulos/dash.php');

        require base_path('routes/modulos/productos/productos.php');
        require base_path('routes/modulos/servicios/servicios.php');
        
        require base_path('routes/modulos/usuarios.php');
        require base_path('routes/modulos/admin/categorias.php');

        
// });
