<?php 

use App\Http\Controllers\Api\Admin\UsuariosController;

    Route::get('/usuarios',         		[UsuariosController::class, 'index']);
    Route::get('/usuarios/buscar/{text}',	[UsuariosController::class, 'search']);
    Route::get('/usuarios/filtrar/{filtro}/{text}',	[UsuariosController::class, 'filter']);
    Route::post('/usuario',         		[UsuariosController::class, 'store']);
    Route::get('/usuario/{id}',     		[UsuariosController::class, 'read']);
    Route::delete('/usuario/{id}',          [UsuariosController::class, 'delete']);

    Route::get('/cliente',  		        [UsuariosController::class, 'cliente']);


?>