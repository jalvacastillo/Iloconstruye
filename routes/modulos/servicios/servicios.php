<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\Servicios\ServiciosController;


Route::get('/servicios',         		    [ServiciosController::class, 'index']);
Route::get('/servicio/{id}',     		    [ServiciosController::class, 'read']);
Route::post('/servicio',                    [ServiciosController::class, 'store']);
Route::delete('/servicio/{id}',  		    [ServiciosController::class, 'delete']);


?>