<?php

    use Illuminate\Support\Facades\Route;
    use App\Http\Controllers\Api\Productos\ProductosController;


    Route::get('/productos',         		    [ProductosController::class, 'index']);
    Route::get('/producto/{id}',     		    [ProductosController::class, 'read']);
    Route::get('/productos/buscar/{text}',      [ProductosController::class, 'search']);
    Route::post('/producto',                    [ProductosController::class, 'store']);
    Route::delete('/producto/{id}',  		    [ProductosController::class, 'delete']);


?>