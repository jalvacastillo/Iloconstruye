<?php 

use App\Http\Controllers\Api\DashController;
// Dash

	Route::get('/dash',   		[DashController::class, 'index']);

    Route::get('/countries',     [DashController::class, 'countries']);