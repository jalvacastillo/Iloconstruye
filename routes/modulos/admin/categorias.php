<?php 

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\Admin\CategoriasController;

    Route::get('/categorias',               [CategoriasController::class, 'index']);
    Route::get('/categoria/{id}',           [CategoriasController::class, 'read']);
    Route::get('/categorias/buscar/{text}', [CategoriasController::class, 'search']);
    Route::post('/categoria',               [CategoriasController::class, 'store']);
    Route::delete('/categoria/{id}',        [CategoriasController::class, 'delete']);


?>