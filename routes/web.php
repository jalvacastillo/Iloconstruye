<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\HomeController;
use App\Http\Controllers\CuentaController;
use App\Http\Controllers\CarritoController;
use App\Http\Controllers\ProductosController;
use App\Http\Controllers\TiendaController;
use App\Http\Controllers\Auth\SocialAuthController;
use App\Http\Controllers\PaymentsController;

use Illuminate\Http\Request;

// Route::group(['middleware' => ['lang']], function () {


Route::get('/',                     [HomeController::class, 'index'])->name('home');

// Nosotros
    Route::get('/quienes-somos',                 [HomeController::class, 'nosotros'])->name('nosotros');
    Route::get('/servicios',                [HomeController::class, 'servicios'])->name('servicios');
    Route::get('/proyectos',                [HomeController::class, 'proyectos'])->name('proyectos');
    Route::get('/bienes-raices',            [HomeController::class, 'raices'])->name('raices');
    Route::get('/ebienes',                  [HomeController::class, 'raices'])->name('raices');

    Route::get('/terminos-y-condiciones',   [HomeController::class, 'terminos'])->name('terminos');
    Route::get('/politicas-de-privacidad',  [HomeController::class, 'privacidad'])->name('privacidad');
    Route::get('/manejo-de-datos',          [HomeController::class, 'datos'])->name('datos');
    Route::get('/cookies',                  [HomeController::class, 'cookies'])->name('cookies');
    Route::get('/envios',                   [HomeController::class, 'envios'])->name('envios');
    Route::get('/devoluciones',             [HomeController::class, 'devoluciones'])->name('devoluciones');
    Route::get('/faqs',                     [HomeController::class, 'faqs'])->name('faqs');

    Route::get('/contactos',            [HomeController::class, 'contactos'])->name('contactos');
    Route::post('/form',                [HomeController::class, 'form'])->name('form');
    

Route::get('/administrador', function(){
    return redirect('/admin');
})->name('admin');
    
// });

// Route::get('lang/{lang}', function ($lang) { session(['lang' => $lang]); return \Redirect::back(); })->where(['lang' => 'en|es']);

Route::get('/admin/{url1?}/{url2?}/{url3?}', function () { return redirect('/admin'); });

// Auth
    Auth::routes();
    Route::get('auth/{provider}',           [SocialAuthController::class, 'redirectToProvider'])->name('social.auth');
    Route::get('auth/{provider}/callback',  [SocialAuthController::class, 'handleProviderCallback']);
