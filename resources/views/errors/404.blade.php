<!DOCTYPE html>
<html lang="{{ Config::get('app.locale') }}" ng-app="app">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Floristería, arreglos, decoraciones, globos y más.">
    <meta name="keywords" content="ropa, zapatos, cremas, maquillaje">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">

    <title>Mágicos Detalles | @yield('title', 'Tienda')</title>

    <!-- Google Font -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@800&family=Poppins:wght@100;400&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Satisfy&display=swap" rel="stylesheet">
    <!-- Css Styles -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/432b1ad5f3.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}" type="text/css">

</head>

<body>
    
    <section class="container mt-5">
        <div class="row text-center justify-content-center">
            <div class="col-md-8">
                <img class="my-5" src="{{ asset('img/logo.png') }}" alt="logo Aleska" width="150px;">
                <h2>
                    <span style="color: #690f42;">Wooups!</span>
                    <br> 
                    <small>No hemos encontrado lo que buscas</small>
                </h2>
                <h4 class="my-5"><a class="btn btn-outline-primary" href="{{ route('catalogo') }}">Ir a la tienda</a></h4>
            </div>
            <div class="col-md-8 mt-5">
                <p id="datos" class="d-flex justify-content-between">
                    <a href="https://www.facebook.com/M%C3%A1gicos-detalles-2310172102583582" rel="nofollow" target="_blank" class="text-white"><i class="fa fa-facebook fa-fw text-white"></i> Facebook</a>
                    <a href="https://www.facebook.com/M%C3%A1gicos-detalles-2310172102583582" rel="nofollow" target="_blank" class="text-white"><i class="fa fa-instagram fa-fw text-white"></i> Instagram</a>
                    <a href="https://www.instagram.com/magicosdetalles" rel="nofollow" target="_blank" class="text-white"><i class="fab fa-facebook-messenger fa-fw text-white"></i> Messenger</a>
                    <a href="https://wa.me/50370086093" rel="nofollow" target="_blank" class="text-white"><i class="fa fa-whatsapp fa-fw text-white"></i> Whatsapp</a>
                </p>
            </div>
        </div>
    </section>

</body>
</html>
