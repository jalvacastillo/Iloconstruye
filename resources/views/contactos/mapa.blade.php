<section class="contact-section section_padding">
  <div class="container">
    <div class="d-none d-sm-block">
      <div id="map" style="height: 480px;"></div>
      <script>
        function initMap() {
          var uluru = {lat: 13.85, lng: -88.85};
          var grayStyles = [
            {
              featureType: "all",
              stylers: [
                { saturation: -90 },
                { lightness: 50 }
              ]
            },
            {elementType: 'labels.text.fill', stylers: [{color: '#ccdee9'}]}
          ];
          var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: 13.85, lng: -88.85},
            zoom: 13,
            styles: grayStyles,
            scrollwheel:  false
          });
        }
        
      </script>
      <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDpfS1oRGreGSBU5HHjMmQ3o5NLw7VdJ6I&callback=initMap"></script>
      
    </div>
  </div>
</section>
