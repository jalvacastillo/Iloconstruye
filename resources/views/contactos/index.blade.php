@extends('layout')

@section('content')

    @include('navbar')

    @include('contactos.header')
    @include('contactos.mapa')
    @include('contactos.form')

    @include('footer')

@endsection