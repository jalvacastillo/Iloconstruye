@extends('layout')

@section('title') Nosotros @endsection

@section('content')

    @include('navbar')

    @include('nosotros.header')
    @include('home.nosotros')
    @include('home.experiencia')
    @include('home.clientes')

    @include('footer')

@endsection