<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="UTF-8">
    <title>Formulario</title>
</head>

<body style="font-family: arial, helvetica; color: #555; background-color: #eee;" bgcolor="#eee">
    
    <section style="width: 95%; text-align: center; border-radius: 15px; background-color: #fff; margin: auto; padding: 10px;">
        <header>
            <img src="{{ asset('/images/logo.png') }}" width="150" alt="Logo IEL">
        </header>
        <div class="margin" style="width: 75%; margin: 30px auto; border: .1px solid #eee;"></div>
        <article style="width: 95%; text-align: justify; margin: auto;">
            
            <p style="margin: 5px;"><b>Escribio</b>: {{ $request->nombre }}</p>
            
            <p style="margin: 5px;"><b>Su teléfono es</b>: {{ $request->telefono }}</p>
            
            <p style="margin: 5px;"><b>Quiere: </b>{{ $request->categoria }}</p>

            <p style="margin: 5px;"><b>Para: </b>{{ $request->fecha }}</p>

            <br><br><br>
            <a target="_blank" href="tel:{{ $request->telefono }}">Llamar</a>
            <br>
            <br>
            <a target="_blank" href="https://wa.me/{{ $request->telefono }}">Enviar Whatsapp</a>
                        
        </article>
        <div class="margin" style="width: 75%; margin: 30px auto; border: .1px solid #eee;"></div>
        <footer>
            <p style="margin: 5px;">Aleska &copy; {{ date('Y') }}</p>
        </footer>
    </section>

</body>
</html>
