<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body style="font-family: sans-serif;">

<div style="padding: 15px;">
    

<table style="width: 100%;">
    <tr>
        <td style="text-align: center;">
            <img width="100px" src="{{ asset('img/logo.png') }}" alt="logo diana's gallery">
            <div style="border-bottom: 1px solid #cecece;">
        </td>
    </tr>
    <tr>
        <td style="text-align: center; padding-top: 10px; padding-bottom: 15px;">
            <img width="100px" src="{{ asset('img/noti.png') }}" alt="logo diana's gallery">
        </td>
    </tr>
    <tr>
        <td style="padding: 50px 25px; background-color: #9e9e9e47; border-radius: 30px; margin: 15px 0px;">
            <h3 style="margin: 0px;">{{ $data['titulo'] }}</h3>

            <p style="margin: 15px 0px;">{{ $data['descripcion'] }}</p>
        </td>
    </tr>
</table>
<table style="width: 100%;">
    
    <tr>
        <td style="text-align: center;">
            <a style="display: block; margin: 20px 0px; text-decoration: none; color: white; background-color: gray; border-radius: 20px; padding: 20px 15px; " href="{{ route('admin') }}">
                Ver la orden
            </a>
        </td>
    </tr>
    <tr>
        <td>
            <div style="border-bottom: 1px solid #cecece;">
        </td>
    </tr>
    <tr>
        <td style="text-align: center;">
            <a href="https://www.facebook.com/Aleskaboutique.sv" target="_blank"><img width="32" height="32" src="https://d2fi4ri5dhpqd1.cloudfront.net/public/resources/social-networks-icon-sets/t-only-logo-dark-gray/facebook@2x.png" alt="Facebook" title="Facebook" style="text-decoration: none;"></a>
            <a href="https://www.instagram.com/aleskaboutique" target="_blank"><img width="32" height="32" src="https://d2fi4ri5dhpqd1.cloudfront.net/public/resources/social-networks-icon-sets/t-only-logo-dark-gray/instagram@2x.png" alt="Instagram" title="Instagram" style="text-decoration: none;"></a>
        </td>
    </tr>
     <tr>
        <td>
            <p style="margin: 15px 0px; text-align: center; color: gray;">
                Aleska | © {{ date('Y')}}
            </p>
            <p style="text-align: center;"><a style="text-decoration: none; color: #848484;" href="{{ route('terminos') }}" target="_blank" rel="noopener">Terminos &amp; Condiciones</a> <strong>|</strong> <a style="text-decoration: none; color: #848484;" href="{{ route('devoluciones') }}" target="_blank" rel="noopener">Politicas de devoluciones</a></p>
        </td>
    </tr>
</table>

</div>

</body>
</html>