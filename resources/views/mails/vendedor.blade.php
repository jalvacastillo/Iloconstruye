<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="UTF-8">
    <title>Formulario</title>
</head>

<body style="font-family: arial, helvetica; color: #555; background-color: #eee;" bgcolor="#eee">
    
    <section style="width: 95%; text-align: center; border-radius: 15px; background-color: #fff; margin: auto; padding: 10px;">
        <header>
            <img src="{{ asset('img/logo.png') }}" width="150" alt="Logo Aleska">
        </header>
        <div class="margin" style="width: 75%; margin: 30px auto; border: .1px solid #eee;"></div>
        <article style="width: 95%; text-align: justify; margin: auto;">
            
            <p style="margin: 5px;"><b>Solicitud para Distribuidor</b></p>

            <p style="margin: 5px;"><b>Nombre</b>: {{ $request->nombre }}</p>
            
            <p style="margin: 5px;"><b>Correo: </b>{{ $request->correo }}</p>

            <p style="margin: 5px;"><b>Teléfono: </b>{{ $request->telefono }}</p>

            <p style="margin: 5px;"><b>Nota: </b>{{ $request->mensaje }}</p>

                        
        </article>
        <div class="margin" style="width: 75%; margin: 30px auto; border: .1px solid #eee;"></div>
        <footer>
            <p style="margin: 5px;">Aleska &copy; {{ date('Y') }}</p>
        </footer>
    </section>

</body>
</html>
