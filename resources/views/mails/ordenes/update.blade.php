<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body style="font-family: sans-serif;">

<div style="padding: 5px;">
    

<table style="width: 100%;">
    <tr>
        <td style="text-align: center; padding-top: 10px; padding-bottom: 15px;">
            <img width="100px" src="{{ asset('img/noti.png') }}" alt="logo Aleska">
        </td>
    </tr>
    <tr>
        <td style="padding: 50px 25px; background-color: #ffdcda; border-radius: 30px; margin: 15px 0px;">
            <h3 style="margin: 0px;">Hola, {{ $orden->cliente->name }}</h3>
            <p style="margin: 15px 0px;">Estado del pedido: 
                <span style="color: #fff; background: #f68d4c; padding: 5px 15px; border-radius: 70px;">{{ $orden->estado }}</span>
            </p>
            @if ($orden->numero_guia)
            <p>
                Número de seguimiento: <b>{{ $orden->numero_guia }}</b>
            </p>
            @endif
            <table style="width: 100%;">
                <tr>
                    <td width="50%" style="padding-right: 15px;">
                        <p><b>Información de envío:</b></p>
                        {{ $orden->direccionFacturacion()->first()->nombre_recibe }} <br>
                        {{ $orden->direccionFacturacion()->first()->codigo_postal }}, {{ $orden->direccionFacturacion()->first()->ciudad }} <br>
                        {{ $orden->direccionFacturacion()->first()->codigo_pais }}
                    </td>
                    <td width="50%" style="padding-right: 15px;">
                        <p><b>Información del pedido:</b></p>
                        N°: {{ $orden->id }} <br>
                        Fecha: {{ $orden->created_at->format('d/m/Y h:m:s a') }} <br>
                        Pagado por: {{ $orden->metodo_pago }}
                    </td>
                </tr>
                <tr>
                    <td><h3>Detalle de tu pedido:</h3></td>
                </tr>
                <tr>
                    <table style="width: 100%;">
                        @foreach ($orden->detalles()->get() as $detalle)
                        <tr>
                            <td width="25%">
                                <img style="width: 100%;" src="{{ asset('img/'. $detalle->img) }}" alt="{{ $detalle->producto_nombre }}">
                            </td>
                            <td width="50%" style="padding: 0px 25px;">
                                {{ $detalle->producto_nombre}} <br>
                                {{ $detalle->cantidad }} x $ {{ $detalle->precio }}
                                @foreach ($detalle->atributos as $atributo)
                                    <span style="display: block;">{{ $atributo->atributo }}:
                                        @if ($atributo->atributo != 'Color')
                                            {{ $atributo->valor }}
                                        @else
                                            <span style="background: {{ strtolower($atributo->valor) }}; width: 13px; height: 13px; display: inline-block; border-radius: 50%; margin-bottom: -2px;"></span>
                                        @endif
                                    </span>
                                @endforeach
                            </td>
                            <td width="25%"  style="text-align: right;">
                                $ {{ $detalle->total}}
                            </td>
                        </tr>
                        @endforeach
                        <tr>
                            <td></td>
                            <td style="text-align: right;">Sub-total:</td>
                            <td style="text-align: right;">${{ $orden->subtotal }}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td style="text-align: right;">Envío:</td>
                            <td style="text-align: right;">${{ $orden->envio }}</td>
                        </tr>
                        @if ($orden->descuento)
                        <tr>
                            <td></td>
                            <td style="text-align: right;">Descuento:</td>
                            <td style="text-align: right;">${{ $orden->descuento }}</td>
                        </tr>
                        @endif
                        <tr>
                            <td></td>
                            <td style="text-align: right;">Impuestos:</td>
                            <td style="text-align: right;">${{ $orden->iva }}</td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <div style="padding-top: 10px; margin-bottom: 10px; border-bottom: 1px solid #cecece;">
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td style="text-align: right;">Total:</td>
                            <td style="text-align: right;">${{ $orden->total }}</td>
                        </tr>
                    </table>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table style="width: 100%;">
    
    <tr>
        <td style="text-align: center;">
            <a style="display: block; margin: 20px 0px; text-decoration: none; color: white; background-color: #af005f; border-radius: 20px; padding: 20px 15px; " href="{{ route('admin') }}">
                Ver la orden
            </a>
        </td>
    </tr>
    <tr>
        <td>
            <div style="border-bottom: 1px solid #cecece;">
        </td>
    </tr>
    <tr>
        <td style="text-align: center;">
            <a href="https://www.facebook.com/DianasGalleryShop" target="_blank"><img width="32" height="32" src="https://d2fi4ri5dhpqd1.cloudfront.net/public/resources/social-networks-icon-sets/t-only-logo-dark-gray/facebook@2x.png" alt="Facebook" title="Facebook" style="text-decoration: none;"></a>
            <a href="https://www.instagram.com/dianasgalleryonline/" target="_blank"><img width="32" height="32" src="https://d2fi4ri5dhpqd1.cloudfront.net/public/resources/social-networks-icon-sets/t-only-logo-dark-gray/instagram@2x.png" alt="Instagram" title="Instagram" style="text-decoration: none;"></a>
        </td>
    </tr>
     <tr>
        <td>
            <p style="margin: 15px 0px; text-align: center; color: gray;">
                Aleska | © {{ date('Y')}}
            </p>
            <p style="text-align: center;"><a style="text-decoration: none; color: #848484;" href="{{ route('terminos') }}" target="_blank" rel="noopener">Terminos &amp; Condiciones</a> <strong>|</strong> <a style="text-decoration: none; color: #848484;" href="{{ route('devoluciones') }}" target="_blank" rel="noopener">Politicas de devoluciones</a></p>
        </td>
    </tr>
</table>

</div>

</body>
</html>