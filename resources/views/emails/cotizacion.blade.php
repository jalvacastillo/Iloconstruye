<!DOCTYPE html>
<html lang="es">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta charset="UTF-8">
	<title>Cotización</title>
</head>

<body style="font-family: arial, helvetica; color: #555; background-color: #eee;" bgcolor="#eee">
	
	<section style="width: 95%; text-align: center; border-radius: 15px; background-color: #fff; margin: auto; padding: 10px;">
		<header>
			<img src="{{ asset('imgs/logo.png') }}" width="150" alt="Logo Iloconstruye">
		</header>
		<div class="margin" style="width: 75%; margin: 30px auto; border: .1px solid #eee;"></div>
	    <article style="width: 95%; text-align: justify; margin: auto;">
	    	
		    <p style="margin: 5px;"><b>Escribio</b>: {{ $request->nombre }}</p>
	    	
	    	<p style="margin: 5px;"><b>Su correo es</b>: {{ $request->correo }}</p>
	    	@if($request->telefono)
	    		<p style="margin: 5px;"><b>Su telefono es</b>: {{ $request->telefono }}</p>
	    	@endif
	    	<p style="margin: 5px;"><b>Nota</b>:{{ $request->mensaje }}</p>
	    	
	    </article>
		<div class="margin" style="width: 75%; margin: 30px auto; border: .1px solid #eee;"></div>
		<footer>
	    	<a href="mailto:{{ $request->correo }}" style="text-decoration: none; background-color: red; color: #fff; padding: 10px 20px;">Responder</a>
	    	<br><br>
			<p style="margin: 5px;">Iloconstruye &amp;copy 2017</p>
		</footer>
	</section>

</body>
</html>
