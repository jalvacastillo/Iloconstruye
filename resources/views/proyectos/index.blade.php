@extends('layout')

@section('content')

    @include('navbar')

    @include('proyectos.header')
    @include('home.proyectos')

    @include('footer')

@endsection