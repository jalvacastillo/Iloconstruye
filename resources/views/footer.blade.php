 <footer class="footer-area">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-sm-6 col-md-4 ">
                    <div class="single-footer-widget footer_1">
                        <a href="{{ route('home') }}"> <img src="{{ asset('img/logo-w.png') }}" alt="" width="150px"> </a>
                        <p>Te diseñamos, construimos o remodelamos tu casa de forma rápida, segura y económica.</p>
                    </div>
                </div>
                <div class=" col-sm-6 col-md-2">
                    <div class="single-footer-widget footer_2">
                        <h4>Enlaces</h4>
                        <div class="contact_info">
                            <ul>
                                <li>
                                    <a href="{{ route('nosotros') }}">Nosotros</a>
                                </li>
                                <li>
                                    <a href="{{ route('proyectos') }}">Proyectos</a>
                                </li>
                                <li>
                                    <a href="{{ route('servicios') }}">Servicios</a>
                                </li>
                                <li>
                                    <a href="{{ route('raices') }}">Bienes Raices</a>
                                </li>
                                <li>
                                    <a href="{{ route('contactos') }}">Contactos</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
              {{--   <div class=" col-sm-6 col-md-4">
                    <div class="single-footer-widget footer_2">
                        <h4>Our Gallery</h4>
                        <div class="footer_img">
                            <a href="#"><img src="img/footer_img/footer_1.png" alt=""></a>
                            <a href="#"><img src="img/footer_img/footer_2.png" alt=""></a>
                            <a href="#"><img src="img/footer_img/footer_3.png" alt=""></a>
                            <a href="#"><img src="img/footer_img/footer_4.png" alt=""></a>
                            <a href="#"><img src="img/footer_img/footer_5.png" alt=""></a>
                            <a href="#"><img src="img/footer_img/footer_6.png" alt=""></a>
                            <a href="#"><img src="img/footer_img/footer_7.png" alt=""></a>
                            <a href="#"><img src="img/footer_img/footer_8.png" alt=""></a>
                        </div>
                    </div>
                </div> --}}
                <div class="col-4 col-sm-6 col-md-4">
                    <div class="single-footer-widget footer_2">
                        <h4>Contactos</h4>
                        <div class="contact_info"> 
                            <p><span> Address :</span> Ilobasco, Cabañas. </p>
                            <p><span> Phone :</span> +503 6058-7693</p>
                            <p><span> Email : </span>info@iloconstruye.com </p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="copyright_part_text text-center">
                        <div class="row">
                            <div class="col-lg-12">
                                <p class="footer-text m-0">
                                    {{ date('Y') }}
                                Derechos reservados | Hecho con <i class="ti-heart" aria-hidden="true"></i> por <a href="https://websis.me" target="_blank">Websis.me</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>