@extends('base')

@section('contents')
<section class="subheader-section position-relative forma-bottom set-bg" data-setbg="/img/bg.jpg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2>@lang('content.resetpass')</h2>
                <div class="breadcrumb__links">
                    <a href="{{ route('home') }}"><i class="fa fa-home"></i> @lang('content.home')</a>
                    <span>@lang('content.resetpass')</span>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container py-5">
    <div class="row justify-content-center mb-5">
        <div class="col-md-7 col-lg-5">
        <form method="POST" action="{{ route('password.update') }}">
            @csrf
            <input type="hidden" name="token" value="{{ $token }}">
            <div class="card">
            <article class="card-body">
                            
                <div class="form-group">
                    <label>@lang('content.email')</label>
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label>@lang('content.password')</label>
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label>Confirmar contraseña</label>
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block"> @lang('content.resetpass')  </button>
                </div>
            </article>
            </div>                                                  
        </form>
        </div>
    </div>
</div>
@endsection