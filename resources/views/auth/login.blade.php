@extends('base')

@section('contents')
<div class="container py-5 mt-5">
    <div class="row mt-5 justify-content-center mb-5">
        <div class="col-md-7 col-lg-5">
        <form method="POST" action="{{ route('login') }}">
            @csrf

            <div class="card">
            <article class="card-body">
                <a href="{{ route('register') }}" class="float-right btn btn-outline-primary">@lang('content.register')</a>
                <h4 class="card-title mb-4 mt-1">@lang('content.signin')</h4>
                <div class="form-group">
                    <label>@lang('content.email')</label>
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <a class="float-right" href="{{ route('password.request') }}">@lang('content.passforgot')</a>
                    <label>@lang('content.password')</label>
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group"> 
                <div class="checkbox">
                  <label> <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}> Recordarme </label>
                </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block"> @lang('content.login')  </button>
                </div>
                <div class="form-group text-center">
                    o ingresa con
                </div>
                <hr>
                <p class="d-flex">
                    <a href="{{ route('social.auth', 'google') }}" class="rounded-0 col btn btn-outline-info"> <i class="fa fa-google"></i> Google</a>
                    <a href="{{ route('social.auth', 'facebook') }}" class="rounded-0 col btn btn-outline-primary"> <i class="fa fa-facebook"></i> Facebook</a>
                </p>
            </article>
            </div>                                                  
        </form>
        </div>
    </div>
</div>
@endsection
