@extends('layout')

@section('content')

    @include('navbar')

    @include('raices.header')
    @include('home.proyectos')

    @include('footer')

@endsection