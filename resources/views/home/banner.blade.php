<section class="banner_part">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-7 col-xl-7">
                <div class="banner_text">
                    <div class="banner_text_iner">
                        <h1>Nosotros <br> 
                            <b><span class="rotating" style="color: #f29d0e;"> diseñamos, construimos, remodelamos, vendemos </span></b>
                            <br>
                        tu casa.</h1>
                        {{-- <p></p> --}}
                        <a href="{{ route('servicios') }}" class="btn_1">Ver servicios </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>