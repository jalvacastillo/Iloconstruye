<section class="review_part section_padding">
        <div class="container-fluid">
            <div class="row align-items-center justify-content-end">
                <div class="col-lg-5 col-xl-4">
                    <div class="tour_pack_text">
                        <h2>Nuestros Clientes</h2>
                        <p>Ellos nos respaldan e inspiran para ofrecerle la garantía de que cada dolar invertido en su proyecto se transformará en su sueño hecho realidad con su casa nueva y a su gusto.</p>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-12">
                    <div class="review_part_cotent owl-carousel">
                        <div class="single_review_part">
                            <img src="img/client/client_2.png" alt="">
                            <div class="tour_pack_content">
                                <p>Life open fifth midst lesser place light after unto move that make had void and whales. So after void called  whose were cattle fourth seed Image yielding is given every own tree Image</p>
                                <h4>Sawpalo, Brasil</h4>
                            </div>
                        </div>
                        <div class="single_review_part">
                            <img src="img/client/client_1.png" alt="">
                            <div class="tour_pack_content">
                                <p> Life open fifth midst lesser place light after unto move that make had void and whales. So after void called  whose were cattle fourth seed Image yielding is given every own tree Image</p>
                                <h4>Sawpalo, Brasil</h4>
                            </div>
                        </div>
                        <div class="single_review_part">
                            <img src="img/client/client_2.png" alt="">
                            <div class="tour_pack_content">
                                <p>Life open fifth midst lesser place light after unto move that make had void and whales. So after void called  whose were cattle fourth seed Image yielding is given every own tree Image</p>
                                <h4>Sawpalo, Brasil</h4>
                            </div>
                        </div>
                        <div class="single_review_part">
                            <img src="img/client/client_1.png" alt="">
                            <div class="tour_pack_content">
                                <p>Life open fifth midst lesser place light after unto move that make had void and whales. So after void called  whose were cattle fourth seed Image yielding is given every own tree Image</p>
                                <h4>Sawpalo, Brasil</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>