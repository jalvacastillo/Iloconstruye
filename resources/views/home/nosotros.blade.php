 <section class="about_part section_padding">
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-md-6 col-lg-6">
                    <div class="about_part_img">
                        <img src="img/about_part_img.png" alt="">
                    </div>
                </div>
                <div class="col-md-6 col-lg-5">
                    <div class="about_part_text">
                        <h2>Construye tus Sueños con Nosotros</h2>
                        <p><b>Somos un equipo especializado</b> en diseño arquitectónico, supervisión y desarrollo de proyectos de remodelación y construcción con una propuesta de diseño profesional de bajo costo y fácil lectura.</p>
                        <ul>
                            <li>
                                <span class="flaticon-drop"></span>
                                <h3>Certified Company</h3>
                                <p>Be man air male shall under create light together grass fly dat also also his brought itself air abundantly </p>
                            </li>
                            <li>
                                <span class="flaticon-ui"></span>
                                <h3>Experience Employee</h3>
                                <p>Be man air male shall under create light together grass fly do also also his brought itself air abundantly </p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>