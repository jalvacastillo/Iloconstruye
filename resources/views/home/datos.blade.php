<section class="member_counter padding_bottom">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-sm-6">
                <div class="single_counter_icon">
                    <img src="img/icon/Icon_1.svg" alt="">
                </div>
                <div class="single_member_counter">
                    <span class="counter">60</span>
                    <h4> <span>Satisfied</span> Client</h4>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="single_counter_icon">
                    <img src="img/icon/Icon_2.svg" alt="">
                </div>
                <div class="single_member_counter">
                    <span class="counter">10</span>
                    <h4> <span>Worldwide</span> Branches</h4>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="single_counter_icon">
                    <img src="img/icon/Icon_3.svg" alt="">
                </div>
                <div class="single_member_counter">
                    <span class="counter">80</span>
                    <h4> <span>Total</span> Projects</h4>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="single_counter_icon">
                    <img src="img/icon/Icon_4.svg" alt="">
                </div>
                <div class="single_member_counter">
                    <span class="counter">24</span>
                    <h4> <span>Work</span> Finished</h4>
                </div>
            </div>
        </div>
    </div>
</section>