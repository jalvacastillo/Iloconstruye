 <section class="our_service padding_top">
        <div class="container">
            <div class="row">
                <div class="col-xl-5">
                    <div class="section_tittle">
                        <h2>Nuestros servicios</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-xl-4">
                    <div class="single_feature mb-3">
                        <div class="single_service">
                            <span class="flaticon-ui"></span>
                            <h4>Diseño Arquitectónico</h4>
                            <p>Te creamos los planos arquitectónicos y 3D para que puedas ver la obra ya terminada, también te ayudamos con los trámites legales garantizando el total cumplimiento de tus expectativas.</p>
                            <a href="#" class="btn_3">read more</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xl-4">
                    <div class="single_feature mb-3">
                        <div class="single_service">
                                <span class="flaticon-ui"></span>
                            <h4>Remodelaciones</h4>
                            <p>Si necesitas transformar, ampliar o sacarle mayor provecho al espacio disponible con el que cuentas nosotros te ofrecemos nuestra experiencia en remodelaciones de interiores y exteriores.</p>
                            <a href="#" class="btn_3">read more</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xl-4">
                    <div class="single_feature mb-3">
                        <div class="single_service">
                                <span class="flaticon-ui"></span>
                            <h4>Presupuestos</h4>
                            <p>Es la parte más importante de cualquier proyecto, nosotros te ayudamos en la creación de presupuestos profesionales de mano de obra y de materiales hechos a la medida de tus necesidades y capacidades.</p>
                            <a href="#" class="btn_3">read more</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xl-4">
                    <div class="single_feature mb-3">
                        <div class="single_service">
                                <span class="flaticon-ui"></span>
                            <h4>Construcción</h4>
                            <p>Ofrecemos los servicios de supervisión y ejecución de obras de construcción para todo tipo de proyectos que requieran conceptos innovadores, arquitectura de marca de alto nivel de viabilidad económica, estética y funcional.</p>
                            <a href="#" class="btn_3">read more</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xl-4">
                    <div class="single_feature mb-3">
                        <div class="single_service">
                                <span class="flaticon-ui"></span>
                            <h4>Diseño 3D</h4>
                            <p>Ofrecemos los servicios de supervisión y ejecución de obras de construcción para todo tipo de proyectos que requieran conceptos innovadores, arquitectura de marca de alto nivel de viabilidad económica, estética y funcional.</p>
                            <a href="#" class="btn_3">read more</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xl-4">
                    <div class="single_feature mb-3">
                        <div class="single_service">
                                <span class="flaticon-ui"></span>
                            <h4>Bienes Raices</h4>
                            <p>Ofrecemos los servicios de supervisión y ejecución de obras de construcción para todo tipo de proyectos que requieran conceptos innovadores, arquitectura de marca de alto nivel de viabilidad económica, estética y funcional.</p>
                            <a href="#" class="btn_3">read more</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>