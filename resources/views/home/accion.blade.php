<section id="subscribe">
  <div class="container wow fadeInUp">
    <div class="row">
      <div class="col-md-8">
        <h3 class="subscribe-title">Solicítanos una cotización</h3>
        <p class="subscribe-text">
            Nos alegrará poder ayudarte y trabajar para tí, cuentanos lo que necesitas sin compromiso .
        </p>
      </div>
      <div class="col-md-4 subscribe-btn-container">
        <a class="subscribe-btn" href="#contact">Cotizar</a>
      </div>
    </div>
  </div>
</section>