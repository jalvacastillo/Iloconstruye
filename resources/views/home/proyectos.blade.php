<section class="our_project section_padding" id="portfolio">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-lg-5 col-sm-10">
                    <div class="section_tittle">
                        <h2>Nuestros Proyectos</h2>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-10">
                    <div class="filters portfolio-filter project_menu_item">
                        <ul>
                            <li class="active" data-filter="*">All</li>
                            <li data-filter=".buildings">Construcción</li>
                            <li data-filter=".rebuild">Remodelación</li>
                            <li data-filter=".architecture">Diseño</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="filters-content">
                <div class="row justify-content-between portfolio-grid">
                    <div class="col-lg-4 col-sm-6 all buildings">
                        <div class="single_our_project">
                            <div class="single_offer">
                                <img src="{{ asset('img/proyectos/ferreteria/1.JPG') }}" alt="offer_img_1">
                                <div class="hover_text">
                                    <p>Proyecto finalizado</p>
                                    <a href="#"><h2>Construcción</h2></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 all rebuild">
                        <div class="single_our_project">
                            <div class="single_offer">
                                <img src="{{ asset('img/proyectos/ferreteria/2.JPG') }}" alt="offer_img_1">
                                <div class="hover_text">
                                    <p>Proyecto finalizado</p>
                                    <a href="#"><h2>Construcción</h2></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 all architecture">
                        <div class="single_our_project">
                            <div class="single_offer">
                                <img src="{{ asset('img/proyectos/ferreteria/3.JPG') }}" alt="offer_img_1">
                                <div class="hover_text">
                                    <p>Proyecto finalizado</p>
                                    <a href="#"><h2>Construcción</h2></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 all buildings">
                        <div class="single_our_project">
                            <div class="single_offer">
                                <img src="{{ asset('img/proyectos/ferreteria/4.JPG') }}" alt="offer_img_1">
                                <div class="hover_text">
                                    <p>Proyecto finalizado</p>
                                    <a href="#"><h2>Construcción</h2></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 all rebuild">
                        <div class="single_our_project">
                            <div class="single_offer">
                                <img src="{{ asset('img/proyectos/ferreteria/5.JPG') }}" alt="offer_img_1">
                                <div class="hover_text">
                                    <p>Proyecto finalizado</p>
                                    <a href="#"><h2>Construcción</h2></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 all architecture">
                        <div class="single_our_project">
                            <div class="single_offer">
                                <img src="{{ asset('img/proyectos/ferreteria/6.JPG') }}" alt="offer_img_1">
                                <div class="hover_text">
                                    <p>Proyecto finalizado</p>
                                    <a href="#"><h2>Construcción</h2></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>