<section id="team">
  <div class="container wow fadeInUp">
    
    <div class="row">
      <div class="col-xs-12">
        <h3 class="section-title">Sobre mí</h3>
        <div class="section-title-divider"></div>
        <p class="section-description">
            Conoce nuestro perfil profesional.
        </p>
      </div>
    </div>

    <div class="row">
      <div class="col-md-6">
        <div class="member">
          <div class="pic"><img src="img/team-1.jpg" alt=""></div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="member">
          <h3>Santos Isidro Alvarado</h3>
          <h5>Gerente de proyectos</h5>
          <p>
            Desde años he trabajado en la supervición y ejecución de más de 35 proyectos de construcción de diferentes tipos y tamaños. <br> Cuento con un diplomado en arquitectura, en diseño 3D y con más de 25 años de experiencia. <br> Buscamos darte calidad al menor costo, construimos de forma profesional sin perjudicar tu bolsillo.
          </p>
          <div class="social">
            <a href="https://www.facebook.com/isidro.alvarado.332" class="btn btn-primary"><i class="fa fa-facebook"></i></a>
          </div>
        </div>
      </div>


    </div>
  </div>
</section>