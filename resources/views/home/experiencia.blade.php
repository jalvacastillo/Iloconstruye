<section class="about_part experiance_part section_padding">
    <div class="container">
        <div class="row align-items-center justify-content-between">
            <div class="col-md-6 col-lg-6">
                <div class="about_part_text">
                    <h2>Somos experiencia en construcción</h2>
                    <p>Contamos con <b>20 años de experiencia</b> profesional en los que hemos realizado <b>más de 120 proyectos</b> de construcción de diferentes tipos y tamaños de manera continua.</p>
                    <div class="about_text_iner">
                        <div class="about_text_counter">
                            <h2>20</h2>
                        </div>
                        <div class="about_iner_content">
                            <h3>años <span>de Experiencia</span></h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-6">
                <div class="about_part_img">
                    <img src="img/experiance_img.png" alt="">
                </div>
            </div>
        </div>
    </div>
</section>