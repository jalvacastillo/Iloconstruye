@extends('layout')

@section('content')

    @include('navbar')

    @include('home.banner')
    @include('home.nosotros')
    @include('home.servicios')
    @include('home.experiencia')
    {{-- @include('home.accion') --}}
    @include('home.proyectos')
    @include('home.clientes')
    {{-- @include('home.contactos') --}}

    @include('footer')

@endsection