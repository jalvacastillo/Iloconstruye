<section id="contact">
  <div class="container wow fadeInUp">
    <div class="row">
      <div class="col-12">
        <h3 class="section-title">Contactanos</h3>
        <div class="section-title-divider"></div>
        <p class="section-description">
            Nos alegrará poder ayudarte y trabajar para tí, dinos lo que necesitas sin compromiso.
        </p>
      </div>
    </div>

    <div class="row justify-content-center">
      <div class="col-12 col-sm-4">
        <div class="info">
          <div>
            <i class="fa fa-map-marker"></i>
            <p>Ilobasco <br>Cabañas, El Salvador.</p>
          </div>

          <div>
            <i class="fa fa-envelope"></i>
            <p>info@iloconstruye.com</p>
          </div>

          <div>
            <i class="fa fa-phone"></i>
            <p>(+503) 7856-1611</p>
          </div>

        </div>
      </div>

      <div class="col-12 col-sm-4">

            <form action="/mensaje" method="POST" role="form" class="contactForm">
            {{ csrf_field() }}
              @if(Session::has('message'))
                  <div class="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <strong>Oh! </strong> {{ Session::get('message') }}
                  </div>
              @endif
            <div class="form-group mb-3">
              <input type="text" name="nombre" class="form-control" placeholder="Como te llamas" required/>
            </div>
            <div class="form-group mb-3">
              <input type="email" class="form-control" name="correo" placeholder="Tu correo para comunicarnos con tigo" required/>
            </div>
            <div class="form-group mb-3">
              <input type="tel" class="form-control" name="telefono" placeholder="Teléfono (Opcional)"/>
            </div>
            <div class="form-group mb-3">
              <textarea class="form-control" name="mensaje" rows="5" placeholder="Cuentanos lo que necesitas!"></textarea>
              <div class="validation"></div>
            </div>
            
            <div class="form-group mb-3">
              <button type="submit" class="btn btn-primary">Enviar</button>
            </div>
            </form>

            </form>
            </div>
      </div>

    </div>
  </div>
</section>