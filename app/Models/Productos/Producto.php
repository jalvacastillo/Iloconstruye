<?php

namespace App\Models\Productos;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Producto extends Model {

    protected $table = 'productos';
    protected $fillable = array(
        'nombre',
        'img',
        'descripcion',
        'detalle',
        'precio',
        'categoria_id',
        'etiquetas',
        'activo',
        'orden',
    );

    protected $appends = ['nombre_categoria'];

    public function apply(Builder $builder, Model $model)
    {
       $builder->where('activo', true);
    }

    public function getEtiquetasAttribute($value) 
    {
        return is_string($value) ? json_decode($value) : $value;
    }
    
    public function getNombreCategoriaAttribute()
    {
        return $this->categoria()->pluck('nombre')->first();
    }

    public function relaciones(){
        return Producto::where('categoria_id', $this->categoria_id)
                        ->where('id', '!=', $this->id)->take(4)->get();
    }

    public function categoria(){
        return $this->belongsTo('App\Models\Admin\Categoria','categoria_id');
    }

}