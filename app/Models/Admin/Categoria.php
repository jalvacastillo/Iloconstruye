<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
	protected $table = 'categorias';
	protected $fillable = array(
        'nombre',
        'img',
        'orden',
	);

	protected $appends = ['total_productos'];

	public function getTotalProductosAttribute()
	{
	    return $this->totalProductos();
	}

	public function totalProductos()
	{
	    return $this->productos()->count();
	}

	public function productos(){
	    return $this->hasMany('App\Models\Productos\Producto','categoria_id');
	}



}
