<?php

namespace App\Models\Servicios;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Servicio extends Model {

    protected $table = 'servicios';
    protected $casts = ['activo' => 'boolean'];
    protected $fillable = array(
        'nombre',
        'img',
        'descripcion',
        'detalle',
        'precio',
        'activo',
        'orden',
    );


}