<?php

namespace App\Listeners;

use Carbon\Carbon;

use Illuminate\Support\Facades\Mail;
use App\Mail\Notificacion;
use App\Mail\OrdenCreated;
use App\Mail\OrdenUpdated;

class OrdenEventSubscriber
{
    /**
     * Handle user login events.
     */
    public function handleOrdenCreated($event) {
        Mail::to(env('MAIL_FROM_ADDRESS'))->send(new Notificacion(['titulo' => 'Nueva orden', 'descripcion' => 'Se ha recibido una orden en la tienda.']));
        Mail::to($event->cliente()->first()->email)->send(new OrdenCreated($event));
    }

    /**
     * Handle user logout events.
     */
    public function handleOrdenUpdate($event) {
        // Admin
        // Mail::to(env('MAIL_FROM_ADDRESS'))->send(new Notificacion(['titulo' => 'Nueva orden', 'descripcion' => 'Se ha recibido una orden en la tienda.']));
        // Cliente
        Mail::to($event->cliente()->first()->email)->send(new OrdenUpdated($event));

    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'eloquent.created: App\Models\Carritos\Orden',
            [OrdenEventSubscriber::class, 'handleOrdenCreated']
        );

        $events->listen(
            'eloquent.updated: App\Models\Carritos\Orden',
            [OrdenEventSubscriber::class, 'handleOrdenUpdate']
        );
    }

}
