<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Session;
use Mail;

class HomeController extends Controller
{

    public function index(){

        return view('home.index');

    }

    public function nosotros(){

        return view('nosotros.index');

    }

    public function servicios(){

        return view('servicios.index');

    }

    public function servicio($slug){

        $servicio = Servicio::where('nombre', 'like', '%'.\Str::replace('-', ' ', $slug).'%')->firstOrFail();

        return view('servicios.servicio.index', compact('servicio'));

    }


    public function proyectos(){

        return view('proyectos.index');

    }


    public function raices(){

        return view('raices.index');

    }


    public function contactos(){

        return view('contactos.index');

    }

    public function terminos(){

        return view('politicas.terminos');

    }

    public function privacidad(){

        return view('politicas.privacidad');

    }

    public function datos(){

        return view('politicas.datos');

    }

    public function envios(){

        return view('politicas.envios');

    }

    public function devoluciones(){

        return view('politicas.devoluciones');

    }

    public function faqs(){

        $faqs = Faq::orderBy('orden', 'desc')->get();

        return view('politicas.faqs', compact('faqs'));

    }

    public function cookies(){

        return view('politicas.cookies');

    }



    public function form(Request $request){

        $request->validate([
            'nombre'    => 'required|max:255',
            'correo'    => 'required|email|max:255',
            'mensaje'   => 'required',
        ]);


        try {
            
            Mail::send('mails.form', ['request' => $request], function ($m) use ($request) {
                $m->from('info@aleska.com', 'Aleska')
                ->to('info@aleska.com', 'Aleska')
                ->replyTo($request->correo)
                ->cc('alvarado.websis@gmail.com')
                ->subject('Mensaje de la Web');
            });

            return response()->json($request, 200);

        } catch (Exception $e) {

            return response()->json($e, 500);
            
        }
        
    }

    public function mensaje(Request $request){
        try {
            
            Mail::send('emails.cotizacion', ['request' => $request], function ($m) use ($request) {
                $m->from('info@iloconstruye.com', 'iloconstruye.com')
                ->to('info@iloconstruye.com', 'iloconstruye.com')
                ->cc('alvarado.websis@gmail.com')
                ->subject('iloconstruye.com');
            });


            Session::flash('message', 'Gracias por escribirnos, te responderemos pronto!');
            return redirect('/#contact');

        } catch (Exception $e) {
            
            Session::flash('message', 'No se pudo enviar, intente nuevamente!');
            return redirect('/#contact');
            
        }

        }
    
}
