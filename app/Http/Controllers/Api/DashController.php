<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use stdClass;
use App\Models\Productos\Producto;
use App\Models\Servicios\Servicio;
use App\Models\Admin\Categoria;

class DashController extends Controller
{
    

    public function index(){
        $datos = new stdClass();


        $datos->Tproductos        = Producto::count();
        $datos->Tservicios        = Servicio::count();
        $datos->Tcategorias       = Categoria::count();

        return Response()->json($datos, 200);

    }


}
