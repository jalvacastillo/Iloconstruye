<?php

namespace App\Http\Controllers\Api\Productos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Productos\Producto;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class ProductosController extends Controller
{
    

    public function index() {
       
        $productos = Producto::orderBy('orden','desc')->get();

        return Response()->json($productos, 200);

    }

    public function search($txt) {

        $productos = Producto::where('nombre', 'like' ,'%' . $txt . '%')->get();
        return Response()->json($productos, 200);

    }

    public function read($id) {

        $producto = Producto::where('id', $id)->firstOrFail();
                            
        return Response()->json($producto, 200);

    }


    public function store(Request $request)
    {

        $request->validate([
            'nombre'        => 'required|max:255|unique:productos,nombre,'. $request->id,
            'file'          => 'sometimes|image|mimes:jpeg,png,jpg|max:5048',
            'descripcion'   => 'max:255',
            'detalle'       => 'max:10000',
            'precio'        => 'numeric',
            'categoria_id'  => 'required|numeric',
            'activo'        => 'boolean'
        ]);

        if($request->id)
            $producto = Producto::findOrFail($request->id);
        else
            $producto = new Producto;
        
        $producto->fill($request->all());

        if ($request->hasFile('file')) {
            if ($request->id && $producto->img && $producto->img != 'productos/default.jpg') {
                Storage::delete($producto->img);
            }
            $path   = $request->file('file');
            $resize = Image::make($path)->resize(720,960)->encode('jpg', 90);
            $hash = md5($resize->__toString());
            $path = "productos/{$hash}.jpg";
            $resize->save(public_path('img/'.$path), 50);
            $producto->img = "/" . $path;
        }


        $producto->save();

        return Response()->json($producto, 200);

    }

    public function delete($id)
    {
        $producto = Producto::findOrFail($id);
        if ($producto->img) {
            Storage::delete($producto->img);
        }
        $producto->delete();

        return Response()->json($producto, 201);

    }

}
