<?php

namespace App\Http\Controllers\Api\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use App\Models\Admin\Categoria;

class CategoriasController extends Controller
{
    
    public function index() {
       
        $categorias = Categoria::orderBy('orden', 'desc')->get();

        return Response()->json($categorias, 200);

    }

    public function search($txt) {

        $categorias = Categoria::where('nombre', 'like' ,'%' . $txt . '%')->get();
        return Response()->json($categorias, 200);

    }


    public function read($id) {

        $categoria = Categoria::findOrFail($request->id);
        return Response()->json($categoria, 200);

    }


    public function store(Request $request)
    {
        $request->validate([
            'nombre'    => 'required|max:255|unique:categorias,nombre,'. $request->id,
            'file'      => 'sometimes|image|mimes:jpeg,png,jpg|max:5048',
            'orden'     => 'numeric',
        ]);

        if($request->id)
            $categoria = Categoria::findOrFail($request->id);
        else
            $categoria = new Categoria;

        $categoria->fill($request->all());

        if ($request->hasFile('file')) {
            if ($request->id && $categoria->img && $categoria->img != 'categorias/default.jpg') {
                Storage::delete($categoria->img);
            }
            $path   = $request->file('file');
            $resize = Image::make($path)->resize(350,350)->encode('jpg', 75);
            $hash = md5($resize->__toString());
            $path = "categorias/{$hash}.jpg";
            $resize->save(public_path('img/'.$path), 50);
            $categoria->img = "/" . $path;
        }

        $categoria->save();

        return Response()->json($categoria, 200);

    }

    public function delete($id)
    {
        $categoria = Categoria::findOrFail($id);
        if ($categoria->img && $categoria->img != 'categorias/default.jpg') {
            Storage::delete($categoria->img);
        }
        $categoria->delete();

        return Response()->json($categoria, 201);

    }


}
