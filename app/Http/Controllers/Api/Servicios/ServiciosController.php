<?php

namespace App\Http\Controllers\Api\Servicios;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Servicios\Servicio;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class ServiciosController extends Controller
{
    

    public function index() {
       
        $servicios = Servicio::orderBy('orden','desc')->get();

        return Response()->json($servicios, 200);

    }

    public function read($id) {

        $servicio = Servicio::where('id', $id)->firstOrFail();
                            
        return Response()->json($servicio, 200);

    }


    public function store(Request $request)
    {

        $request->validate([
            'nombre'        => 'required|max:255|unique:categorias,nombre,'. $request->id,
            'file'          => 'sometimes|image|mimes:jpeg,png,jpg|max:5048',
            'descripcion'   => 'max:255',
            'detalle'       => 'max:10000',
            'precio'        => 'numeric',
            'orden'         => 'numeric',
            'activo'        => 'boolean'
        ]);

        if($request->id)
            $servicio = Servicio::findOrFail($request->id);
        else
            $servicio = new Servicio;
        
        $servicio->fill($request->all());

        if ($request->hasFile('file')) {
            if ($request->id && $servicio->img && $servicio->img != 'servicios/default.jpg') {
                Storage::delete($servicio->img);
            }
            $path   = $request->file('file');
            $resize = Image::make($path)->resize(720,660)->encode('jpg', 90);
            $hash = md5($resize->__toString());
            $path = "servicios/{$hash}.jpg";
            $resize->save(public_path('img/'.$path), 50);
            $servicio->img = "/" . $path;
        }

        $servicio->save();

        return Response()->json($servicio, 200);

    }

    public function delete($id)
    {
        $servicio = Servicio::findOrFail($id);
        $servicio->delete();

        return Response()->json($servicio, 201);

    }

}
